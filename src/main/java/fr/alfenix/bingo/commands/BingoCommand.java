package fr.alfenix.bingo.commands;

import fr.alfenix.bingo.Bingo;
import fr.alfenix.bingo.configuration.ConfigManager;
import fr.alfenix.bingo.managers.Game;
import fr.alfenix.bingo.utils.Utils;
import lombok.NonNull;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BingoCommand implements CommandExecutor {

    /**
    * Called when the /bingo command is executed
    *
    * @param sender The sender of the command
    * @param cmd The command
    * @param label The command label
    * @param args The arguments given with the command
    * @return True if valid command
    */
    @Override
    public boolean onCommand(@NonNull CommandSender sender, @NonNull Command cmd, @NonNull String label, @NonNull String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Player player = (Player) sender;

        String arg = args.length > 0 ? args[0] : "";
        if (arg.equals("start")) {
            if (!Bingo.getInstance().getBingoManager().isPlayerInGame(player)) {
                Bingo.getInstance().getBingoManager().start(player);
            } else {
                player.sendMessage(Bingo.prefix(ConfigManager.getMessagesConfig().getAlreadyPlaying()));
            }
            return true;
        }
        if (arg.equals("stop")) {
            if (Bingo.getInstance().getBingoManager().isPlayerInGame(player)) {
                Bingo.getInstance().getBingoManager().stop(player);
            } else {
                player.sendMessage(Bingo.prefix(ConfigManager.getMessagesConfig().getNotPlaying()));
            }
            return true;
        }
        if (arg.equals("display")) {
            if (Bingo.getInstance().getBingoManager().isPlayerInGame(player)) {
                Game playerGame = Bingo.getInstance().getBingoManager().getPlayerGame(player);
                if (playerGame != null) {
                    playerGame.displayGrid(player);
                }
            } else {
                player.sendMessage(Bingo.prefix(ConfigManager.getMessagesConfig().getNotPlaying()));
            }
            return true;
        }
        ConfigManager.getMessagesConfig().getBingoInfo().forEach(s -> player.sendMessage(Utils.translateColor(s)));
        return true;
    }
}
