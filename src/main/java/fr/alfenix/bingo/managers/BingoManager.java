package fr.alfenix.bingo.managers;

import fr.alfenix.bingo.Bingo;
import fr.alfenix.bingo.configuration.ConfigManager;
import fr.alfenix.bingo.tasks.GameLaunchTask;
import fr.alfenix.bingo.utils.Utils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class BingoManager {

    @Getter
    private List<Material> materials;
    @Getter
    private List<Game> games;

    /**
     * Initializes the game
     */
    public void init() {
        materials = new ArrayList<>();
        List<Material> banList = ConfigManager.getBingoConfig().getBanList()
                .stream().map(Material::valueOf).collect(Collectors.toList());
        Arrays.stream(Material.values()).filter(m -> !banList.contains(m)).forEach(materials::add);

        games = new ArrayList<>();
    }

    /**
     * Starts a new game with available players
     * @param player The player who run the start command
     */
    public void start(Player player) {
        List<Player> availablePlayers = Bingo.getInstance().getBingoManager().getAvailablePlayers();
        int nbPlayers = ConfigManager.getBingoConfig().getNbPlayers();
        if (!availablePlayers.contains(player)) {
            player.sendMessage();
        }
        if (availablePlayers.size() < nbPlayers) {
            player.sendMessage(Utils.translateColor(Bingo.prefix(ConfigManager.getMessagesConfig()
                    .getNotEnoughPlayer().replace("{nbPlayers}", String.valueOf(nbPlayers)))));
            return;
        }
        createGame();
        Game game = games.get(games.size()-1);
        new GameLaunchTask(game).runTaskTimer(Bingo.getInstance(), 0, 20L);
    }

    /**
     * Creates a new game instance
     */
    public void createGame() {
        Game game = new Game(materials);
        games.add(game);
    }

    /**
     * Gets all online players that are not playing
     * @return The list of players
     */
    public List<Player> getAvailablePlayers() {
        List<Player> availablePlayers = new ArrayList<>();
        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            boolean available = true;
            for (Game game : games) {
                if (game.getPlayerMap().containsKey(onlinePlayer.getUniqueId())) {
                    available = false;
                }
            }
            if (available) {
                availablePlayers.add(onlinePlayer);
            }

        }
        return availablePlayers;
    }


    /**
     * Checks if the player is playing
     * @param player The player
     * @return true if the player is playing
     */
    public boolean isPlayerInGame(Player player) {
        return games.stream().anyMatch(game -> game.getPlayerMap().containsKey(player.getUniqueId()));
    }

    @Nullable
    public Game getPlayerGame(Player player) {
        return games.stream().filter(game -> game.getPlayerMap().containsKey(player.getUniqueId())).findFirst().orElse(null);
    }

    @Nullable
    public PlayerData getPlayerData(Player player) {
        return Objects.requireNonNull(games.stream().filter(game -> game.getPlayerMap().containsKey(player.getUniqueId())).findFirst().orElse(null)).getPlayerMap().get(player.getUniqueId());
    }

    /**
     * Stops the game of the player
     * @param player The player who stop the game
     */
    public void stop(Player player) {
        Game game = getPlayerGame(player);
        if (game != null) {
            game.stop();
            games.remove(game);
        }

    }

    /**
     * Finish the game
     * @param game The game
     */
    public void finish(Game game) {
        games.remove(game);
    }
}
