package fr.alfenix.bingo.managers;

import fr.alfenix.bingo.Bingo;
import fr.alfenix.bingo.configuration.ConfigManager;
import fr.alfenix.bingo.inventories.GridInventory;
import fr.alfenix.bingo.tasks.GameLaunchTask;
import fr.alfenix.bingo.tasks.PlayingTask;
import fr.alfenix.bingo.utils.Utils;
import fr.alfenix.bingo.utils.WorldUtils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/**
 * Bingo
 */
public class Game {

    private final GameState gameState;

    private GridInventory gridInventory;

    @Getter
    private final Map<UUID, PlayerData> playerMap;

    public Game(List<Material> materialList) {
        this.gameState = GameState.WAITING;
        this.playerMap = new HashMap<>();
        this.init(materialList);
    }

    /**
     * Initializes the game
     * @param materialList The list of materials to generate the grid
     */
    public void init(List<Material> materialList) {
        gridInventory = new GridInventory(materialList);
        gameState.setState(GameState.WAITING);

    }

    /**
     * Starts the game with available players
     */
    public void start() {
        List<Player> players = Bingo.getInstance().getBingoManager().getAvailablePlayers();
        for (Player player : players) {
            if (players.size() < ConfigManager.getBingoConfig().getNbPlayers()) {
                    player.sendMessage(Utils.translateColor(Bingo.prefix(
                            ConfigManager.getMessagesConfig().getNotEnoughPlayer().replace("{nbPlayers}",
                                    String.valueOf(ConfigManager.getBingoConfig().getNbPlayers())))));
            } else {
                player.sendMessage(Bingo.prefix(ConfigManager.getMessagesConfig().getBingoStart()));
                playerMap.put(player.getUniqueId(), new PlayerData(player, gridInventory));
                teleportPlayers();
                player.setHealth(20.0);
                player.setInvulnerable(true);
                Bukkit.getScheduler().runTaskLater(Bingo.getInstance(), () -> {
                    player.setInvulnerable(false);
                    player.sendMessage(Bingo.prefix(ConfigManager.getMessagesConfig().getNoLongerImmune()));
                }, 200L);
                player.getInventory().clear();
                player.setGameMode(GameMode.SURVIVAL);
            }
        }
        gameState.setState(GameState.PLAYING);
        new PlayingTask(this).runTaskTimer(Bingo.getInstance(), 0, 20L);
    }


    /**
     * Checks if the item picked up by the player is a non obtained item of the grid
     * @param player The player
     * @param event The pickup item event
     */
    public void onPickedUpItem(Player player, EntityPickupItemEvent event) {
        ItemStack itemStack = event.getItem().getItemStack();
        if (!gameState.isState(GameState.PLAYING)) {
            return;
        }
        if (playerMap.get(player.getUniqueId()).getGridInventory().checkItemStack(itemStack)) {
            playerMap.get(player.getUniqueId()).incrementNbItems();
            event.setCancelled(true);
            event.getItem().remove();
            playerMap.keySet().forEach(player1 -> player.sendMessage(Bingo.prefix(
                    ConfigManager.getMessagesConfig().getPlayerFoundItem()
                            .replace("{player}", player.getDisplayName())
                            .replace("{item}", itemStack.getType().name().replace("_", " ")
            ))));
            checkGridLines(player);
        }
    }

    /**
     * Checks if the player has completed new line in the grid
     * @param player The player
     */
    public void checkGridLines(Player player) {
        PlayerData playerData = playerMap.get(player.getUniqueId());
        if (playerData == null) {
            return;
        }
        int nbLines = playerData.getGridInventory().getNbLinesCompleted();
        while (playerData.getNbLineCompleted() < nbLines) {
            playerData.incrementNbLineCompleted();
            gameMessage(ConfigManager.getMessagesConfig().getLineCompleted()
                .replace("{player}", player.getDisplayName())
                .replace("{numberCompleted}", String.valueOf(playerData.getNbLineCompleted()))
                .replace("{numberToComplete}", String.valueOf(ConfigManager.getBingoConfig().getLineNumber())));
        }
        if (nbLines >= ConfigManager.getBingoConfig().getLineNumber()) {
            gameMessage(ConfigManager.getMessagesConfig().getPlayerWon().replace("{player}", player.getDisplayName()));
            gameState.setState(GameState.FINISHED);
            Bingo.getInstance().getBingoManager().finish(this);
        }
    }


    /**
     * Displays the grid to the player
     * @param player The player
     */
    public void displayGrid(Player player) {
        playerMap.get(player.getUniqueId()).display();
    }


    /**
     * Teleports all players in the current game to a configured radius
     */
    private void teleportPlayers() {
        int randomAngle = new Random().nextInt(360);
        double increaseAngle = playerMap.size() / 360.0f;
        double radius = ConfigManager.getBingoConfig().getTeleportRadius();

        double playerSpawnAngle;
        List<UUID> players = new ArrayList<>(playerMap.keySet());
        for (int i = 0; i < playerMap.size(); i++) {
            playerSpawnAngle = randomAngle + increaseAngle * i;
            Player player = Bukkit.getPlayer(players.get(i));
            if (player == null) {
                continue;
            }
            double x = radius*Math.sin(Math.toRadians(playerSpawnAngle));
            double z = radius*Math.cos(Math.toRadians(playerSpawnAngle));
            Location playerSpawn = WorldUtils.findSafeBlock(player.getWorld(), (int) x, (int) z);
//            playerMap.get()
            player.teleport(playerSpawn);
        }
    }

    /**
     * Sends a message to all the players of the game with the plugin prefix before
     * @param message The message
     */
    public void gameMessage(String message) {
        playerMap.keySet().stream().map(Bukkit::getPlayer).filter(Objects::nonNull)
                .forEach(player -> player.sendMessage(Bingo.prefix(message)));
    }

    /**
     * Stops the game
     */
    public void stop() {
        for (UUID uuid : playerMap.keySet()) {
            Player player = Bukkit.getPlayer(uuid);
            if (player != null) {
                player.sendMessage(Bingo.prefix(ConfigManager.getMessagesConfig().getGameStopped()));
            }
        }
        gameState.setState(GameState.FINISHED);
    }

    /**
     * Finish the game
     */
    public void finish() {
        int maxLines = 0;
        List<PlayerData> winners = new ArrayList<>();
        for (UUID uuid : playerMap.keySet()) {
            PlayerData playerData = playerMap.get(uuid);
            if (playerData.getNbLineCompleted() == maxLines) {
                winners.add(playerData);
            }
            if (playerData.getNbLineCompleted() > maxLines) {
                maxLines = playerData.getNbLineCompleted();
                winners.clear();
                winners.add(playerData);
            }
        }
        if (winners.size() > 1) {
            List<PlayerData> realWinners = new ArrayList<>();
            int maxItems = 0;
            for (PlayerData playerData : winners) {
                if (playerData.getNbItems() == maxItems) {
                    realWinners.add(playerData);
                }
                if (playerData.getNbItems() > maxItems){
                    maxItems = playerData.getNbItems();
                    realWinners.clear();
                    realWinners.add(playerData);
                }
            }
            winners = realWinners;
        }
        if (winners.size() == 1) {
            gameMessage(ConfigManager.getMessagesConfig().getPlayerWon()
                    .replace("{player}", winners.get(0).getPlayer().getDisplayName()));
        } else {
            gameMessage(ConfigManager.getMessagesConfig().getNumberWon()
                    .replace("{numberOfWinners}", String.valueOf(winners.size())));
        }
        gameState.setState(GameState.FINISHED);
        Bingo.getInstance().getBingoManager().finish(this);
    }
}
