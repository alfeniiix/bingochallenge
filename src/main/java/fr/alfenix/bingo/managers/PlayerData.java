package fr.alfenix.bingo.managers;

import fr.alfenix.bingo.inventories.GridInventory;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

/**
 * The data of a player in game
 */
public class PlayerData {

    @Getter
    private final Player player;
    @Getter
    private final GridInventory gridInventory;

    @Getter
    private int nbLineCompleted;

    @Getter
    private int nbItems;

    public PlayerData(Player player, GridInventory gridInventory) {
        this.player = player;
        this.gridInventory = new GridInventory(gridInventory);
        this.nbLineCompleted = 0;
    }

    /**
     * Displays the grid of the player
     */
    public void display() {
        gridInventory.display(player);
    }

    public void incrementNbItems() {
        nbItems++;
    }

    public void incrementNbLineCompleted() {
        nbLineCompleted++;
    }

}
