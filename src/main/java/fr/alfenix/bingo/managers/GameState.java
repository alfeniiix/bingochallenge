package fr.alfenix.bingo.managers;


import java.util.Arrays;

/**
 * Game states
 */
public enum GameState {

    /**
     * Waiting for players
     */
    WAITING,

    /**
     * Starting the game
     */
    STARTING,

    /**
     * Playing the game
     */
    PLAYING,

    /**
     * Game is finished
     */
    FINISHED,

    /**
     * Preparing next game
     */
    RESTARTING;



    private GameState currentState;

    /**
     * Sets the state of the game
     * @param gameState The game state
     */
    public void setState(GameState gameState){
        currentState = gameState;
    }

    /**
     * Checks if the game is at the given state
     * @param gameStates The gameState
     * @return true if the game is at the given state
     */
    public boolean isState(GameState... gameStates){
        return Arrays.asList(gameStates).contains(currentState);
    }
}
