package fr.alfenix.bingo.utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.stream.Stream;

public class WorldUtils {

    /**
     * Finds a block that is secure enough to teleport player
     * @param world The world to teleport player
     * @param x The approximate x coordinate to find the block
     * @param z The approximate z coordinate to find the block
     * @return The safe block
     */
    public static Location findSafeBlock(World world, int x, int z) {
        Block block = world.getHighestBlockAt(x, z);
        while (isNearToBlockType(block, Material.LAVA) ||
               isNearToBlockType(block.getRelative(0, 1, 0), Material.LAVA) ||
               isNearToBlockType(block.getRelative(0, 2, 0), Material.LAVA)) {
            block = world.getHighestBlockAt(x, z);
            x++;
            z++;
        }
        return new Location(block.getWorld(), block.getX()+0.5, block.getY()+1.0, block.getZ()+0.5);

    }

    /**
     * Checks if the block and the surrounding blocks contain the given material
     * @param block The block to check
     * @param material The material
     * @return true if contain lava
     */
    public static boolean isNearToBlockType(Block block, Material material) {
        return Stream.of(
                block.getRelative(BlockFace.EAST),
                block.getRelative(BlockFace.NORTH),
                block.getRelative(BlockFace.SOUTH),
                block.getRelative(BlockFace.WEST),
                block.getRelative(BlockFace.SOUTH_EAST),
                block.getRelative(BlockFace.SOUTH_WEST),
                block.getRelative(BlockFace.NORTH_WEST),
                block.getRelative(BlockFace.NORTH_EAST)
        ).anyMatch(block1 -> block1.getType().equals(material));
    }

}
