package fr.alfenix.bingo.utils;

import org.bukkit.ChatColor;

import java.util.List;

public class Utils {

    /**
     * Translates color codes of given message
     * @param message The message
     * @return The translated message
     */
    public static String translateColor(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    /**
     * Translates color codes of given list of message
     * @param messages The list of message
     * @return The translated list of message
     */
    public static List<String> translateColor(List<String> messages) {
        for (int i = 0; i < messages.size(); i++) {
            messages.set(i, ChatColor.translateAlternateColorCodes('&', messages.get(i)));
        }
        return messages;
    }

}
