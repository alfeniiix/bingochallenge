package fr.alfenix.bingo.tasks;

import fr.alfenix.bingo.configuration.ConfigManager;
import fr.alfenix.bingo.managers.Game;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Tasks which manage the game timer
 */
public class PlayingTask extends BukkitRunnable {

    private final Game game;
    private int minutes;
    private int seconds;

    public PlayingTask(Game game) {
        this.game = game;
        this.minutes = 5;
        this.seconds = 0;
    }

    @Override
    public void run() {


        if (minutes == 0 && seconds == 0) {
            game.finish();
            this.cancel();
        }
        if (minutes == 0 && seconds < 5) {
            game.gameMessage(ConfigManager.getMessagesConfig().getMinutesLeft()
                    .replace("{minutes}", String.valueOf(minutes)));
        }
        if (seconds == 0) {
            game.gameMessage(ConfigManager.getMessagesConfig().getMinutesLeft()
                    .replace("{minutes}", String.valueOf(minutes)));
//            seconds = 60;
            seconds = 15; // set at 15 to speed up the game
            minutes--;
        }
        seconds--;
    }
}
