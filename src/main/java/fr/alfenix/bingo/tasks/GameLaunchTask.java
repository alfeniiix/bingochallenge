package fr.alfenix.bingo.tasks;

import fr.alfenix.bingo.Bingo;
import fr.alfenix.bingo.configuration.ConfigManager;
import fr.alfenix.bingo.managers.Game;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Tasks which manage the launch timer of the game
 */
public class GameLaunchTask extends BukkitRunnable {

    private final Game game;
    private int timer;

    public GameLaunchTask(Game game) {
        this.game = game;
        this.timer = 5;
    }

    @Override
    public void run() {
        if (timer <= 0) {
            game.start();
            this.cancel();
        } else {
            for (Player availablePlayer : Bingo.getInstance().getBingoManager().getAvailablePlayers()) {
                availablePlayer.sendMessage(Bingo.prefix(
                        ConfigManager.getMessagesConfig().getGameStarting().replace("{x}", String.valueOf(timer))
                ));
            }
        }
        timer--;

    }
}
