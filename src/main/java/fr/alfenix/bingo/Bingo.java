package fr.alfenix.bingo;

import fr.alfenix.bingo.commands.BingoCommand;
import fr.alfenix.bingo.configuration.ConfigManager;
import fr.alfenix.bingo.listeners.PlayerListener;
import fr.alfenix.bingo.managers.BingoManager;
import fr.alfenix.bingo.utils.Utils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class Bingo extends JavaPlugin {

    @Getter
    private static Bingo instance;
    @Getter
    private BingoManager bingoManager;

    @Override
    public void onEnable() {
        instance = this;


        Bukkit.getLogger().info("[Bingo] Loading configuration..");
        ConfigManager.init(this);

        Bukkit.getLogger().info("[Bingo] Loading commands..");
        Objects.requireNonNull(getCommand("bingo")).setExecutor(new BingoCommand());

        Bukkit.getLogger().info("[Bingo] Loading listeners..");
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);

        Bukkit.getLogger().info("[Bingo] Loading managers..");
        bingoManager = new BingoManager();
        bingoManager.init();

    }

    @Override
    public void onDisable() {
        Bukkit.getLogger().info("Disable Bingo");
    }

    /**
     * Gets the color translated message and add the plugin prefix before
     * @param message The message
     * @return The prefix
     */
    public static String prefix(String message) {
        return Utils.translateColor(ConfigManager.getMessagesConfig().getPrefix() + message);
    }
}
