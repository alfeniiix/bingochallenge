package fr.alfenix.bingo.inventories;

import fr.alfenix.bingo.configuration.ConfigManager;
import fr.alfenix.bingo.utils.Utils;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * The grid of the bingo
 */
public class GridInventory {


    /**
     * Width of the grid
     */
    private final int SIZE = 5;

    /**
     * Items of the grid
     */
    @Getter
    private List<ItemStack> itemStacks;

    private List<Boolean> grid;


    public GridInventory(GridInventory gridInventory) {
        itemStacks = gridInventory.getItemStacks();
        this.grid = new ArrayList<>();
        for (int i = 0; i < SIZE * SIZE; i++) {
            grid.add(false);
        }
    }

    public GridInventory(List<Material> materials) {
        generateGrid(materials);
    }


    /**
     * Generates a new grid with a materials list
     * @param materials The material list
     */
    private void generateGrid(List<Material> materials) {
        Random random = new Random();
        List<Material> copy = new ArrayList<>(materials);
        itemStacks = new ArrayList<>(SIZE * SIZE);
        for (int i = 0; i < SIZE * SIZE; i++) {
            Material randomMaterial = copy.remove(random.nextInt(copy.size()-1));
            ItemStack itemStack = new ItemStack(randomMaterial);
            ItemMeta itemMeta = itemStack.getItemMeta();
            if (itemMeta != null) {
                itemMeta.setLore(Utils.translateColor(ConfigManager.getMessagesConfig().getItemNotFoundLore()));
                itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                itemStack.setItemMeta(itemMeta);
            }
            itemStacks.add(itemStack);
        }
    }

    /**
     * Display the grid to a player.
     * @param player The player
     */
    public void display(Player player) {
        Inventory inventory = Bukkit.createInventory(player, 9* SIZE,
                Utils.translateColor(ConfigManager.getMessagesConfig().getBingoInventory()));

        int startWidth = (9 - SIZE) / 2;

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                inventory.setItem(i*9+(j+startWidth), itemStacks.get(i* SIZE +j));
            }
        }
        player.openInventory(inventory);
    }

    /**
     * Checks the grid itemStack.
     * @param itemStack The itemStack
     * @return true if the item is in the grid and if it is obtained for the first time or null.
     */
    public boolean checkItemStack(ItemStack itemStack) {
        for (int i = 0; i < SIZE * SIZE; i++) {
            if (itemStacks.get(i).getType().equals(itemStack.getType()) && !grid.get(i)) {
                grid.set(i, true);
                ItemStack is = itemStacks.get(i);
                is.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 1);
                ItemMeta itemMeta = is.getItemMeta();
                if (itemMeta != null) {
                    itemMeta.setLore(Utils.translateColor(ConfigManager.getMessagesConfig().getItemFoundLore()));
                    itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                    is.setItemMeta(itemMeta);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the number of lines completed in the grid
     * @return the number of lines
     */
    public int getNbLinesCompleted() {
        int nbLines = 0;
        int row;
        int col;
        int diag1 = 0;
        int diag2 = 0;
        for (int i = 0; i < SIZE; i++) {
            row = 0;
            col = 0;
            diag1 += grid.get(i * SIZE + i) ? 1 : 0;
            diag2 += grid.get(i * SIZE + SIZE-i) ? 1 : 0;
            for (int j = 0; j < SIZE; j++) {
                row += grid.get(i * SIZE + j) ? 1 : 0;
                col += grid.get(j * SIZE + i) ? 1 : 0;
            }
            if (row == SIZE) {
                nbLines++;
            }
            if (col == SIZE) {
                nbLines++;
            }
        }
        if (diag1 == SIZE) {
            nbLines++;
        }
        if (diag2 == SIZE) {
            nbLines++;
        }
        return nbLines;
    }
}
