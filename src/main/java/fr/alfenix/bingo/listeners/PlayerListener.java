package fr.alfenix.bingo.listeners;

import fr.alfenix.bingo.Bingo;
import fr.alfenix.bingo.configuration.ConfigManager;
import fr.alfenix.bingo.managers.Game;
import fr.alfenix.bingo.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;

public class PlayerListener implements Listener {

    /**
     * Called when a player pickup an item
     * @param e The event
     */
    @EventHandler
    public void onPickupItem(EntityPickupItemEvent e) {
        if (!(e.getEntity() instanceof Player)) {
            return;
        }
        Player player = (Player) e.getEntity();
        if (Bingo.getInstance().getBingoManager().isPlayerInGame(player)) {
            Game playerGame = Bingo.getInstance().getBingoManager().getPlayerGame(player);
            if (playerGame != null) {
                playerGame.onPickedUpItem(player, e);
            }

        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        checkInventory(e);
    }

    @EventHandler
    public void onDragClick(InventoryDragEvent e) {
        checkInventory(e);
    }

    /**
     * Checks the inventory clicked and cancel the event if it is the grid
     * @param e The event
     */
    private void checkInventory(InventoryInteractEvent e) {
        if (!(e.getWhoClicked() instanceof Player)) {
            return;
        }

        Player player = ((Player) e.getWhoClicked()).getPlayer();
        if (player != null) {
            if (e.getView().getTitle().equals(Utils.translateColor(ConfigManager.getMessagesConfig().getBingoInventory()))) {
                e.setCancelled(true);
            }
        }
    }


}
