package fr.alfenix.bingo.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MessagesConfig {

    /* GLOBAL MESSAGES */

    private String prefix;

    @JsonProperty("bingo-info")
    private List<String> bingoInfo;

    @JsonProperty("game-starting")
    private String gameStarting;

    @JsonProperty("bingo-start")
    private String bingoStart;

    /* GAME MESSAGES */

    @JsonProperty("player-found-item")
    private String playerFoundItem;

    @JsonProperty("item-not-found-lore")
    private List<String> itemNotFoundLore;

    @JsonProperty("item-found-lore")
    private List<String> itemFoundLore;

    @JsonProperty("bingo-inventory")
    private String bingoInventory;

    @JsonProperty("game-stopped")
    private String gameStopped;

    @JsonProperty("line-completed")
    private String lineCompleted;

    @JsonProperty("player-won")
    private String playerWon;

    @JsonProperty("number-won")
    private String numberWon;

    @JsonProperty("no-longer-immune")
    private String noLongerImmune;

    @JsonProperty("minutes-left")
    private String minutesLeft;

    @JsonProperty("seconds-left")
    private String secondsLeft;

    /* ERROR MESSAGES */

    @JsonProperty("not-enough-player")
    private String notEnoughPlayer;

    @JsonProperty("already-playing")
    private String alreadyPlaying;

    @JsonProperty("not-playing")
    private String notPlaying;
}
