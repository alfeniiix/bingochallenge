package fr.alfenix.bingo.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Config Manager
 */
@UtilityClass
public class ConfigManager {

    @Getter
    private BingoConfig bingoConfig;
    @Getter
    private MessagesConfig messagesConfig;


    /**
     * The mapper used to fill the configuration class
     */
    private final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());


    /**
     * Initializes the configurations
     * @param plugin The main plugin
     */
    public void init(@NonNull JavaPlugin plugin) {
        try {
            if (!plugin.getDataFolder().exists()) {
                if (!plugin.getDataFolder().mkdir()) {
                    Bukkit.getLogger().warning("Cannot create directory " + plugin.getDataFolder());
                }
            }
            bingoConfig = readConfig(plugin, "bingo.yml", BingoConfig.class);
            messagesConfig = readConfig(plugin, "messages.yml", MessagesConfig.class);
        } catch (IOException e) {
            Bukkit.getLogger().warning("Error while loading configuration file : " + e);
        }
    }

    /**
     * Fills configuration class using corresponding file
     * @param plugin THe main plugin
     * @param config The content of the configuration file
     * @param clazz The configuration class
     * @return The filled configuration class
     */
    public <T> T readConfig(JavaPlugin plugin, String config, Class<T> clazz) throws IOException {
        File bingoConfigFile = new File(plugin.getDataFolder() + File.separator + config);
        if (!bingoConfigFile.exists()) {
            plugin.saveResource(config, true);
        }
        return mapper.readValue(Files.readAllBytes(bingoConfigFile.toPath()), clazz);
    }

}
