package fr.alfenix.bingo.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Config of the game
 */
@Getter
@Setter
public class BingoConfig {


    private int lineNumber;

    private double teleportRadius;

    private int nbPlayers;

    @JsonProperty("ban-list")
    private List<String> banList;


}
